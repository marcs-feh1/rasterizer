package rasterizer

import "core:math"

distance_v2 :: proc(v0, v1: vec2) -> f32 {
	d := (v0 - v1) * (v0 - v1)
	return math.sqrt(d.x + d.y)
}

close_enough :: proc(val, target, delta: f32) -> bool {
	return (val >= (target - delta)) && (val <= (target + delta))
}
