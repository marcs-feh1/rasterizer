package rasterizer

import "core:fmt"
import intr "core:intrinsics"
import "core:simd"

draw_triangle :: proc {draw_triangle_abc, draw_triangle_vec}
draw_triangle_wireframe :: proc {draw_triangle_wireframe_abc, draw_triangle_wireframe_vec}
raster_pixel :: proc{raster_pixel_xy, raster_pixel_vec}
raster_triangle :: raster_triangle_simd
raster_line :: raster_line_breseham

raster_pixel_xy :: proc(using canvas: ^Canvas, x: int, y:int, col: Color) {
	data[(y * width) + x] = col
}

raster_pixel_vec :: proc(using canvas: ^Canvas, p: vec2i, col: Color) {
	x := int(p.x)
	y := int(p.y)
	data[(y * width) + x] = col
}

canvas_ptr_at :: proc(using canvas: ^Canvas, p: vec2i) -> ^Color {
	x := int(p.x)
	y := int(p.y)
	return &data[(y * width) + x]
}

draw_point :: proc(canvas: ^Canvas, point: vec2, col: Color) {
	p := denormalize_coord(canvas.width, canvas.height, point)
	if p.x >= auto_cast canvas.width || p.y >= auto_cast canvas.height {return}
	raster_pixel(canvas, p, col)
}

draw_line :: proc(canvas: ^Canvas, a, b: vec2, col: Color) {
	a := denormalize_coord(canvas.width, canvas.height, a)
	b := denormalize_coord(canvas.width, canvas.height, b)

	raster_line(canvas, a, b, col)
}

draw_triangle_abc :: proc(canvas: ^Canvas, a, b, c: vec2, col: Color) {
	a := denormalize_coord(canvas.width, canvas.height, a)
	b := denormalize_coord(canvas.width, canvas.height, b)
	c := denormalize_coord(canvas.width, canvas.height, c)
	raster_triangle(canvas, a, b, c, col)
}

draw_triangle_vec :: proc(canvas: ^Canvas, t: [3]vec2, col: Color) {
	a := denormalize_coord(canvas.width, canvas.height, t[0])
	b := denormalize_coord(canvas.width, canvas.height, t[1])
	c := denormalize_coord(canvas.width, canvas.height, t[2])
	raster_triangle(canvas, a, b, c, col)
}

draw_triangle_wireframe_abc :: proc(canvas: ^Canvas, a, b, c: vec2, col: Color) {
	a := denormalize_coord(canvas.width, canvas.height, a)
	b := denormalize_coord(canvas.width, canvas.height, b)
	c := denormalize_coord(canvas.width, canvas.height, c)
	raster_line(canvas, a, b, col)
	raster_line(canvas, b, c, col)
	raster_line(canvas, c, a, col)
}

draw_triangle_wireframe_vec :: proc(canvas: ^Canvas, t: [3]vec2, col: Color) {
	a := denormalize_coord(canvas.width, canvas.height, t[0])
	b := denormalize_coord(canvas.width, canvas.height, t[1])
	c := denormalize_coord(canvas.width, canvas.height, t[2])
	raster_line(canvas, a, b, col)
	raster_line(canvas, b, c, col)
	raster_line(canvas, c, a, col)
}

// Signed distance of a to line defined by bc
orient_2d :: proc(a, b, c: $T/[2]$E) -> E where intr.type_is_numeric(E) {
	ba := b - a
	ca := c - a
	return (ba.x * ca.y) - (ba.y * ca.x)
}

// Signed distance of a to line defined by bc
orient_2d_simd :: proc(ax, ay, bx, by, cx, cy: simd.i32x16) -> simd.i32x16 {
	ba_x := bx - ax
	ba_y := by - ay
	ca_x := cx - ax
	ca_y := cy - ay
	return (ba_x * ca_y) - (ba_y * ca_x)
}


raster_line_breseham :: proc(canvas: ^Canvas, a, b: vec2i, col: Color) {
	dx := abs(a.x - b.x)
	dy := -abs(a.y - b.y)
	sx : i32 = 1 if a.x < b.x else -1
	sy : i32 = 1 if a.y < b.y else -1

	err := dx + dy

	p := a
	for {
		raster_pixel(canvas, p, col)

		if p == b { break }

		err2 := err * 2

		if err2 >= dy {
			if p.x == b.x { return }
			err += dy
			p.x += sx
		}

		if err2 <= dx {
			if p.y == b.y { return }
			err += dx
			p.y += sy
		}
	}
}


raster_triangle_simd :: proc(canvas: ^Canvas, a, b, c: vec2i, col: Color) {
	// Get bounds
	min_x := min(a.x, b.x, c.x)
	min_y := min(a.y, b.y, c.y)
	max_x := max(a.x, b.x, c.x)
	max_y := max(a.y, b.y, c.y)

	// Clamp to bounds
	min_x = max(min_x, 0)
	min_y = max(min_y, 0)
	max_x = min(max_x, i32(canvas.width - 1))
	max_y = min(max_y, i32(canvas.height - 1))

	// TODO: shading
	color_lane: simd.u32x16 = auto_cast transmute(u32)col

	sign := triangle_bias(a, b, c)
	if sign == 0 {return}

	bias: simd.i32x16 = auto_cast sign

	p_x: simd.i32x16
	p_y: simd.i32x16

	x, y: i32
	for y = min_y; y <= max_y; y += 1 {
		p_y = auto_cast y
		for x = min_x; x <= max_x; x += 16 {
			p_x = {x + 0, x + 1, x + 2, x + 3, x + 4, x + 5, x + 6, x + 7, x + 8, x + 9, x + 10, x + 11, x + 12, x + 13, x + 14, x + 15, }

			w0_v := orient_2d_simd(b.x, b.y, c.x, c.y, p_x, p_y) * bias
			w1_v := orient_2d_simd(c.x, c.y, a.x, a.y, p_x, p_y) * bias
			w2_v := orient_2d_simd(a.x, a.y, b.x, b.y, p_x, p_y) * bias

			in0 := simd.lanes_ge(w0_v, auto_cast 0)
			in1 := simd.lanes_ge(w1_v, auto_cast 0)
			in2 := simd.lanes_ge(w2_v, auto_cast 0)
			inside := in0 & in1 & in2

			cv_ptr := cast(^[16]u32)canvas_ptr_at(canvas, {x, y})
			cv_data := simd.from_array(cv_ptr^)

			{ // Mask and write to buffer
				new_px := color_lane & inside
				old_px := cv_data & (~inside)
				cv_ptr^ = simd.to_array(new_px | old_px)
			}
		}
	}

	// TODO: rest
}

raster_triangle_simple :: proc(canvas: ^Canvas, a, b, c: vec2i, col: Color) {
	// Get bounds
	min_x := min(a.x, b.x, c.x)
	min_y := min(a.y, b.y, c.y)
	max_x := max(a.x, b.x, c.x)
	max_y := max(a.y, b.y, c.y)

	// Clamp to bounds
	min_x = max(min_x, 0)
	min_y = max(min_y, 0)
	max_x = min(max_x, i32(canvas.width - 1))
	max_y = min(max_y, i32(canvas.height - 1))

	// Sign bias
	bias := triangle_bias(a, b, c)

	for y in min_y ..= max_y {
		for x in min_x ..= max_x {
			p := vec2i{x, y}

			w0 := orient_2d(b, c, p) * bias
			w1 := orient_2d(c, a, p) * bias
			w2 := orient_2d(a, b, p) * bias

			if (w0 >= 0) && (w1 >= 0) && (w2 >= 0) {
				raster_pixel(canvas, p, col)
			}
		}
	}
}

// Bias to apply depending on winding order of triangle, returns 0 if it's a
// degenerate
@(private = "file")
triangle_bias :: proc(a, b, c: vec2i) -> i32 {
	bias: i32 = 1
	m := matrix[3, 3]i32{
		a.x, a.y, 1, 
		b.x, b.y, 1, 
		c.x, c.y, 1, 
	}
	det := matrix3x3_determinant(m)
	if det == 0 {return 0} // Degenerate triangle
	if det < 0 {
		bias = -1
	}
	return bias
}

