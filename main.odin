package rasterizer

import "core:bytes"
import "core:fmt"
import "core:image"
import "core:math"
import "core:math/rand"
import "core:time"

BG := rgba(0.85, 0.8, 0.75, 1.0)
SAMPLES :: 16

main :: proc() {
	cv := canvas_create(256, 256)
	defer canvas_destroy(&cv)
	for _, i in cv.data {cv.data[i] = BG}

	a, b, c: vec2 = {0, 0.5 * math.SQRT_TWO}, {-1, -0.5}, {0.8, -0.7}
	color := vec4{.8, .6, .2, 1.0}

	draw_triangle_wireframe(&cv, a, b, c, rgba(color))

	err := save_pbm("out.ppm", cv)
	if err != nil {
		fmt.panicf("Failed to save image: %v", err)
	}
}

rand_vec2 :: proc(lo, hi: f32, r: ^rand.Rand = nil) -> vec2 {
	v := vec2{rand.float32_uniform(lo, hi, r), rand.float32_uniform(lo, hi, r)}
	return v
}

rand_vec3 :: proc(lo, hi: f32, r: ^rand.Rand = nil) -> vec3 {
	v := vec3 {
		rand.float32_uniform(lo, hi, r),
		rand.float32_uniform(lo, hi, r),
		rand.float32_uniform(lo, hi, r),
	}
	return v
}
