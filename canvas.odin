package rasterizer

import "core:bytes"
import "core:image"
import intr "core:intrinsics"
import "core:mem"
import "core:slice"

Image :: image.Image

Color :: image.RGBA_Pixel

vec1 :: [1]f32
vec2 :: [2]f32
vec3 :: [3]f32
vec4 :: [4]f32

vec1i :: [1]i32
vec2i :: [2]i32
vec3i :: [3]i32
vec4i :: [4]i32

Canvas :: struct {
	data:   []Color,
	width:  int,
	height: int,
}

canvas_create :: proc(width, height: int) -> Canvas {
	data := make([]Color, width * height)
	return Canvas{data = data, width = width, height = height}
}

canvas_destroy :: proc(canvas: ^Canvas) {
	delete(canvas.data)
}

byte_swap_u32 :: proc(v: u32) -> u32 {
	return transmute(u32)cast(u32be)(v)
}

canvas_to_image :: proc(cv: Canvas) -> Image {
	buf: bytes.Buffer

	buf_data, _ := make([]u8, len(cv.data) * 4)
	_ = mem.copy_non_overlapping(raw_data(buf_data), raw_data(cv.data), len(buf_data))

	bytes.buffer_init(&buf, buf_data)

	img := Image {
		channels = 4,
		depth    = 8,
		width    = cv.width,
		height   = cv.height,
		pixels   = buf,
		which    = .Unknown,
	}

	return img
}

denormalize_coord :: proc(width: int, height: int, point: vec2) -> vec2i {
	cw := f32(width - 1)
	ch := f32(height - 1)

	fx, fy := (cw / 2) + (point.x * (cw / 2)), (ch / 2) - (point.y * (ch / 2))

	return vec2i{i32(fx), i32(fy)}
}

clamp :: proc(min, x, max: $T) -> T {
	if x > max {
		return max
	}
	if x < min {
		return min
	}
	return x
}


color_float4 :: proc(r, g, b: $T, a: T) -> Color where intr.type_is_float(T) {
	return(
		Color {
			auto_cast (r * 0xff),
			auto_cast (g * 0xff),
			auto_cast (b * 0xff),
			auto_cast (a * 0xff),
		} \
	)
}

color_float3 :: proc(r, g, b: $T) -> Color where intr.type_is_float(T) {
	return Color{auto_cast (r * 0xff), auto_cast (g * 0xff), auto_cast (b * 0xff), 0xff}
}

color_vec4 :: proc(v: $T/[4]$E) -> Color where intr.type_is_float(E) {
	return(
		Color {
			auto_cast (v[0] * 0xff),
			auto_cast (v[1] * 0xff),
			auto_cast (v[2] * 0xff),
			auto_cast (v[3] * 0xff),
		} \
	)
}

color_vec3 :: proc(v: $T/[3]$E) -> Color where intr.type_is_float(E) {
	return Color{auto_cast (v[0] * 0xff), auto_cast (v[1] * 0xff), auto_cast (v[2] * 0xff), 0xff}
}

rgba :: proc {
	color_float4,
	color_float3,
	color_vec4,
	color_vec3,
}
