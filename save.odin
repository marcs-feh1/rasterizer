package rasterizer

import "core:fmt"
import pbm "core:image/netpbm"
import "core:os"

save_ppm :: proc(outfile: string, cv: Canvas) {
	format: string : "P6\n%d %d\n255\n"

	header := fmt.tprintf(format, cv.width, cv.height)
	f, _ := os.open(outfile, flags = os.O_WRONLY | os.O_CREATE, mode = 0o644)
	defer os.close(f)

	os.write(f, raw_data(header)[0:len(header)])

	pixbuf := make([][3]u8, len(cv.data))
	defer delete(pixbuf)

	for px, i in cv.data {
		pixbuf[i] = {px.r, px.g, px.b}
	}

	imgdata := (cast([^]u8)(cast(rawptr)raw_data(pixbuf)))[0:len(pixbuf) * 3]
	os.write(f, imgdata)
}

save_pbm :: proc(outfile: string, cv: Canvas) -> (err: pbm.Error) {
	img := canvas_to_image(cv)

	info := pbm.Info {
		header =  {
			format = .P7,
			little_endian = false,
			tupltype = "RGB_ALPHA",
			width = cv.width,
			height = cv.height,
			maxval = 0xff,
		},
	}

	err = pbm.save("out.pbm", &img, custom_info = info)

	return
}
